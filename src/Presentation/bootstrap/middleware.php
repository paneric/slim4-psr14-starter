<?php

declare(strict_types=1);

$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();

<?php

declare(strict_types=1);

use App\Presentation\Commerce\Controller\ArticleController;
use App\Presentation\Commerce\Controller\CartController;

$app->get('/cart/show-all-articles', CartController::class . ':showAllArticles')
    ->setName('commerce.cart.show_all_articles');

$app->get('/article/show-all', ArticleController::class . ':showAll')
    ->setName('commerce.article.show_all');
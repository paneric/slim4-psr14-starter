<?php

declare(strict_types=1);

namespace App\Presentation\Commerce\Controller;

use App\Presentation\Commerce\Service\CartService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class CartController
{
    protected $cartService;//CartService

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function showAllArticles(Request $request, Response $response): Response
    {
        dump($this->cartService->getAllArticles());

        $response->getBody()->write('Hello World');

        return $response;
    }
}

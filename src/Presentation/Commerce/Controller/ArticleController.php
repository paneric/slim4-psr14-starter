<?php

declare(strict_types=1);

namespace App\Presentation\Commerce\Controller;

use App\Presentation\Commerce\Service\ArticleService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ArticleController
{
    protected $articleService;//ArticleService

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function showAll(Request $request, Response $response): Response
    {
        dump($this->articleService->getAll());

        $response->getBody()->write('Hello World');

        return $response;
    }
}

<?php

declare(strict_types=1);

namespace App\Presentation\Commerce\Service;

use App\Presentation\Commerce\Handler\ArticleHandler;

class ArticleService
{
    protected $articleHandler;//ArticleHandler

    public function __construct(ArticleHandler $articleHandler)
    {
        $this->articleHandler = $articleHandler;
    }

    public function getAll(): array
    {
        return [
            'articles' => $this->articleHandler->getAll()
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Presentation\Commerce\Service;

use App\Presentation\Commerce\Handler\CartHandler;

class CartService
{
    protected $cartHandler;//CartHandler

    public function __construct(CartHandler $cartHandler)
    {
        $this->cartHandler = $cartHandler;
    }

    public function getAllArticles(): array
    {
        return [
            'articles' => $this->cartHandler->getAllArticles()
        ];
    }
}

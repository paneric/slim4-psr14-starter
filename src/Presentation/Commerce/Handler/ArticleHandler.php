<?php

declare(strict_types=1);

namespace App\Presentation\Commerce\Handler;

use App\Presentation\Handler;
use App\Gateway\Article\ArticleEvent;

class ArticleHandler extends Handler
{
    public function getAll(): array
    {
        $articleDataEvent = new ArticleEvent();

        return $this->dispatcher->dispatch(
            $articleDataEvent->setMethod('findAll')
        )->getArrayResult();
    }
}

<?php

declare(strict_types=1);

namespace App\Presentation;

use Psr\EventDispatcher\EventDispatcherInterface;

abstract class Handler
{
    protected $dispatcher;//EventDispatcherInterface

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }
}

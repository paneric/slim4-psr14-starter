<?php

declare(strict_types=1);

return [
    // TODO: DEV/PROD update local config:
    'dev' => [
        'name' => 'tribet',
        'default_local' => 'pl',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => '127.0.0.1',
        'security' => false, //true if can be sent only by https
        'http_only' => true, //not accessible for javascript
    ],
    'test' => [
        'name' => 'tribet',
        'default_local' => 'pl',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => 'tribet.pl',
        'security' => false, //true if can be sent only by https
        'http_only' => false, //not accessible for javascript
    ],
    'prod' => [
        'name' => 'tribet',
        'default_local' => 'pl',
        'expire' => time() + 60 * 60 * 24 * 120, // 120 days
        'path' => '/',
        'domain' => 'tribet.pl',
        'security' => true, //true if can be sent only by https
        'http_only' => true, //not accessible for javascript
    ],

];

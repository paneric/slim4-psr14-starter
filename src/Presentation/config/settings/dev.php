<?php

declare(strict_types=1);

return [
    'dbal' => [
        'limit' => 10,
        'host' => 'localhost',
        'charset' => 'utf8',
        'dbName' => 'slim4_psr14_starter',
        'user' => 'user',
        'password' => 'password',
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ],
    ],
];

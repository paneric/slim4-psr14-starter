<?php

declare(strict_types=1);

return [
    'default_route_key' => 'main',
    'local_map' => ['en', 'pl'],
    'module_map' => [
        'article' => 'Commerce',
        'cart' => 'Commerce',
    ],
];

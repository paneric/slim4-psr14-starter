<?php

declare(strict_types=1);

use App\Adapters\Article\ArticleQueryAdapter;
use App\Adapters\Article\ArticleRepositoryAdapter;
use App\Adapters\Credential\CredentialRepositoryAdapter;
use App\DAL\Article\ArticleQuery;
use App\DAL\Article\ArticleRepository;
use App\DAL\Credential\CredentialRepository;
use App\Interfaces\ArticleQueryInterface;
use App\Interfaces\ArticleRepositoryInterface;
use App\Interfaces\CredentialRepositoryInterface;
use DI\Container;


return [
    ArticleRepositoryInterface::class => static function (Container $container): ArticleRepositoryAdapter
    {
        return new ArticleRepositoryAdapter(
            $container->get(ArticleRepository::class)
        );
    },

    ArticleQueryInterface::class => static function (Container $container): ArticleQueryAdapter
    {
        return new ArticleQueryAdapter(
            $container->get(ArticleQuery::class)
        );
    },

    CredentialRepositoryInterface::class => static function (Container $container): CredentialRepositoryAdapter
    {
        return new CredentialRepositoryAdapter(
            $container->get(CredentialRepository::class)
        );
    },
];

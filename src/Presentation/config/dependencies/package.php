<?php

declare(strict_types=1);

use DI\Container;
use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;

return [

    Manager::class => static function (Container $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },
];

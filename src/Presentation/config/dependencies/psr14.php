<?php

use App\Gateway\Article\ArticleEvent;
use App\Gateway\Article\ArticleListenerService;
use App\Interfaces\ArticleQueryInterface;
use App\Interfaces\ArticleRepositoryInterface;
use Crell\Tukio\Dispatcher;
use Crell\Tukio\OrderedListenerProvider;
use DI\Container;
use Psr\EventDispatcher\EventDispatcherInterface;

return [
    EventDispatcherInterface::class => static function (Container $container): Dispatcher
    {
        $provider = new OrderedListenerProvider($container);
        $provider->addListenerService(
            ArticleListenerService::class,
            'execute',
            ArticleEvent::class
        );

        return new Dispatcher($provider);
    },

    ArticleListenerService::class => static function (Container $container): ArticleListenerService
    {
        return new ArticleListenerService(
            $container->get(ArticleRepositoryInterface::class),
            $container->get(ArticleQueryInterface::class)
        );
    },
];

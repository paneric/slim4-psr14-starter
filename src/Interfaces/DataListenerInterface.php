<?php

declare(strict_types=1);

namespace App\Interfaces;

interface DataListenerInterface
{
    public function execute(DataEventInterface $event): void;
}

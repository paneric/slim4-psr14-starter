<?php

declare(strict_types=1);

namespace App\Interfaces;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface RepositoryInterface
{
    public function adaptManager(): void;

    public function findOneBy(array $criteria): ?DataObjectInterface;
    public function findOneById(int $id): ?DataObjectInterface;
    public function findOneByRef(string $ref): ?DataObjectInterface;

    public function findAll(): array;
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;
    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): array;

    public function create(DataObjectInterface $dataObject): string;
    public function update(int $id, DataObjectInterface $dataObject): int;
    public function delete(int $id): int;

    public function createMultiple(array $fieldsSets): int;
    public function updateMultiple(array $fieldsSets): int;
    public function deleteMultiple(array $ids): int;

    public function getRowsNumber(): int;
}

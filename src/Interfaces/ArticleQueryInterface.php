<?php

declare(strict_types=1);

namespace App\Interfaces;

interface ArticleQueryInterface extends QueryInterface
{
    public function queryAll(): array;
}

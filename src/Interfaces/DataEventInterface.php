<?php

declare(strict_types=1);

namespace App\Interfaces;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface DataEventInterface
{
    public function getIdType(): ?string;
    public function setIdType(string $idType): void;

    public function getIntId(): ?int;
    public function setIntId(int $intId): DataEventInterface;

    public function getStrId(): ?string;
    public function setStrId(string $strId): DataEventInterface;

    public function getParams(): ?array;
    public function setParams(array $params): DataEventInterface;
    public function addParam(string $name, $value): DataEventInterface;

    public function getMethod(): ?string;
    public function setMethod(string $method): self;

    public function getObjectResult(): ?DataObjectInterface;
    public function setObjectResult(DataObjectInterface $objectResult): DataEventInterface;

    public function getArrayResult(): ?array;
    public function setArrayResult(array $arrayResult): DataEventInterface;

    public function getIntResult(): ?int;
    public function setIntResult(int $intResult): DataEventInterface;

    public function getStrResult(): ?string;
    public function setStrResult(string $strResult): DataEventInterface;
}

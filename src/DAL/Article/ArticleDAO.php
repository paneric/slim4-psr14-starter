<?php

declare(strict_types=1);

namespace App\DAL\Article;

use Paneric\DataObject\DAO;

class ArticleDAO extends DAO
{
    protected $id;//int
    protected $credentialId;//str
    protected $ref;//ref


    public function __construct()
    {
        $this->prefix = 'art_';

        $this->setMaps();
    }


    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }
    public function setCredentialId(string $credentialId): void
    {
        $this->credentialId = $credentialId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
}

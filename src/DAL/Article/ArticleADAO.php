<?php

declare(strict_types=1);

namespace App\DAL\Article;

use App\DAL\Credential\CredentialDAO;
use Paneric\DataObject\ADAO;

class ArticleADAO extends ADAO
{
    private $article;//ArticleDAO
    private $credential;//CredentialDAO

    public function __construct()
    {
        $this->article = new ArticleDAO();
        $this->values = $this->article->hydrate($this->values);

        $this->credential = new CredentialDAO();
        $this->values = $this->credential->hydrate($this->values);

        unset($this->values);
    }

    public function getArticle(): ArticleDAO
    {
        return $this->article;
    }
    protected function setArticle(ArticleDAO $article): void
    {
        $this->article = $article;
    }

    public function getCredential(): CredentialDAO
    {
        return $this->credential;
    }
    public function setCredential(CredentialDAO $credential): void
    {
        $this->credential = $credential;
    }
}

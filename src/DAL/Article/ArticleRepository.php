<?php

declare(strict_types=1);

namespace App\DAL\Article;

use App\Interfaces\ArticleRepositoryInterface;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;

class ArticleRepository extends Repository implements ArticleRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles';
        $this->daoClass = ArticleDAO::class;
    }
}

<?php

declare(strict_types=1);

namespace App\DAL\Article;

use App\Interfaces\ArticleQueryInterface;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Query;

class ArticleQuery extends Query implements ArticleQueryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->adaoClass = ArticleADAO::class;
    }

    public function queryAll(): array
    {
        $this->adaptManager();

        $query = '
            SELECT *
            FROM articles bt
                INNER JOIN credentials st1 on bt.art_credential_id = st1.crd_id                
        ';

        $stmt = $this->manager->setStmt($query);

        return $stmt->fetchAll();
    }
}

<?php

declare(strict_types=1);

namespace App\DAL\Credential;

use Paneric\DataObject\DAO;

class CredentialDAO extends DAO
{
    protected $id;//str
    protected $ref;//str


    public function __construct()
    {
        $this->prefix = 'crd_';

        $this->setMaps();
    }


    public function getId(): string
    {
        return $this->id;
    }
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getRef(): string
    {
        return $this->ref;
    }
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }
}

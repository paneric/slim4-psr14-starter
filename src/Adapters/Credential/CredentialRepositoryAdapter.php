<?php

declare(strict_types=1);

namespace App\Adapters\Credential;

use App\DAL\Credential\CredentialRepository;
use App\Interfaces\CredentialRepositoryInterface;
use Paneric\Interfaces\DataObject\DataObjectInterface;

class CredentialRepositoryAdapter implements CredentialRepositoryInterface
{
    protected $credentialRepository;//CredentialRepository

    public function __construct(CredentialRepository $credentialRepository)
    {
        $this->credentialRepository = $credentialRepository;
    }


    public function adaptManager(): void
    {
        $this->credentialRepository->adaptManager();
    }

    public function findOneBy(array $criteria): ?DataObjectInterface
    {
        return $this->credentialRepository->findOneBy($criteria);
    }

    public function findOneById(int $id): ?DataObjectInterface
    {
        return $this->credentialRepository->findOneById($id);
    }

    public function findOneByRef(string $ref): ?DataObjectInterface
    {
        return $this->credentialRepository->findOneByRef($ref);
    }

    public function findAll(): array
    {
        return $this->credentialRepository->findAll();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->credentialRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): array
    {
        return $this->credentialRepository->findByEnhanced($criteria, $operator, $id);
    }

    public function create(DataObjectInterface $dataObject): string
    {
        return $this->credentialRepository->create($dataObject);
    }

    public function update(int $id, DataObjectInterface $dataObject): int
    {
        return $this->credentialRepository->update($id, $dataObject);
    }

    public function delete(int $id): int
    {
        return $this->credentialRepository->delete($id);
    }

    public function createMultiple(array $fieldsSets): int
    {
        return $this->credentialRepository->createMultiple($fieldsSets);
    }

    public function updateMultiple(array $fieldsSets): int
    {
        return $this->credentialRepository->updateMultiple($fieldsSets);
    }

    public function deleteMultiple(array $ids): int
    {
        return $this->credentialRepository->deleteMultiple($ids);
    }

    public function getRowsNumber(): int
    {
        return $this->credentialRepository->getRowsNumber();
    }
}

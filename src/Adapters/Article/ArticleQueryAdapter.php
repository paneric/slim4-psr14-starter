<?php

declare(strict_types=1);

namespace App\Adapters\Article;

use App\DAL\Article\ArticleQuery;
use App\Interfaces\ArticleQueryInterface;

class ArticleQueryAdapter implements ArticleQueryInterface
{
    protected $articleQuery;//ArticleQuery

    public function __construct(ArticleQuery $articleQuery)
    {
        $this->articleQuery = $articleQuery;
    }


    public function adaptManager(): void
    {
        $this->articleQuery->adaptManager();
    }

    public function queryAll(): array
    {
        return $this->articleQuery->queryAll();
    }
}

<?php

declare(strict_types=1);

namespace App\Adapters\Article;

use App\DAL\Article\ArticleRepository;
use App\Interfaces\ArticleRepositoryInterface;
use Paneric\Interfaces\DataObject\DataObjectInterface;

class ArticleRepositoryAdapter implements ArticleRepositoryInterface
{
    protected $articleRepository;//ArticleRepository

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }


    public function adaptManager(): void
    {
        $this->articleRepository->adaptManager();
    }

    public function findOneBy(array $criteria): ?DataObjectInterface
    {
        return $this->articleRepository->findOneBy($criteria);
    }

    public function findOneById(int $id): ?DataObjectInterface
    {
        return $this->articleRepository->findOneById($id);
    }

    public function findOneByRef(string $ref): ?DataObjectInterface
    {
        return $this->articleRepository->findOneByRef($ref);
    }

    public function findAll(): array
    {
        return $this->articleRepository->findAll();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->articleRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): array
    {
        return $this->articleRepository->findByEnhanced($criteria, $operator, $id);
    }

    public function create(DataObjectInterface $dataObject): string
    {
        return $this->articleRepository->create($dataObject);
    }

    public function update(int $id, DataObjectInterface $dataObject): int
    {
        return $this->articleRepository->update($id, $dataObject);
    }

    public function delete(int $id): int
    {
        return $this->articleRepository->delete($id);
    }

    public function createMultiple(array $fieldsSets): int
    {
        return $this->articleRepository->createMultiple($fieldsSets);
    }

    public function updateMultiple(array $fieldsSets): int
    {
        return $this->articleRepository->updateMultiple($fieldsSets);
    }

    public function deleteMultiple(array $ids): int
    {
        return $this->articleRepository->deleteMultiple($ids);
    }

    public function getRowsNumber(): int
    {
        return $this->articleRepository->getRowsNumber();
    }
}

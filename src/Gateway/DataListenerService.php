<?php

declare(strict_types=1);

namespace App\Gateway;

use App\Interfaces\DataEventInterface;
use App\Interfaces\DataListenerInterface;

abstract class DataListenerService implements DataListenerInterface
{
    protected $repository;//RepositoryInterface
    protected $query;//QueryInterface

    public function execute(DataEventInterface $event): void
    {
        $this->{$event->getMethod()}($event);
    }

    protected function findAll(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $event->setArrayResult(
            $this->repository->findAll()
        );
    }
    protected function findBy(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setArrayResult(
            $this->repository->findBy(
                $params['criteria'],
                $params['order_by'],
                $params['limit'],
                $params['offset']
            )
        );
    }
    protected function findByEnhanced(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setArrayResult(
            $this->repository->findBy(
                $params['criteria'],
                $params['operator'],
                $event->{'get' . $event->getIdType() . 'Id'}()
            )
        );
    }

    protected function findOneById(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $event->setObjectResult(
            $this->repository->findOneById(
                $event->{'get' . $event->getIdType() . 'Id'}()
            )
        );
    }
    protected function findOneBy(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setObjectResult(
            $this->repository->findOneBy(
                $params['criteria']
            )
        );
    }

    protected function create(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            (int) $this->repository->create(
                $params['attributes']
            )
        );

;
    }
    protected function update(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->update(
                $event->{'get' . $event->getIdType() . 'Id'}(),
                $params['attributes']
            )
        );
    }
    protected function delete(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $event->setIntResult(
            $this->repository->delete(
                $event->{'get' . $event->getIdType() . 'Id'}()
            )
        );
    }

    protected function createMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->createMultiple(
                $params['fields_sets']
            )
        );
    }
    protected function updateMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->updateMultiple(
                $params['fields_sets']
            )
        );
    }
    protected function deleteMultiple(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setIntResult(
            $this->repository->deleteMultiple(
                $params['ids']
            )
        );
    }

    protected function getRowsNumber(DataEventInterface $event): void
    {
        $event->setIntResult(
            $this->repository->getRowsNumber()
        );
    }
}

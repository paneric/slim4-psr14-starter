<?php

declare(strict_types=1);

namespace App\Gateway;

use App\Interfaces\DataEventInterface;
use Paneric\Interfaces\DataObject\DataObjectInterface;

abstract class DataEvent implements DataEventInterface
{
    protected $idType = 'Int';//string(Int,Str)

    protected $intId;//int
    protected $strId;//string

    protected $params;//array
    protected $method;//string

    protected $objectResult;//DataObjectInterface
    protected $arrayResult;//array
    protected $intResult;//int
    protected $strResult;//string


    public function getIdType(): ?string
    {
        return $this->idType;
    }
    public function setIdType(?string $idType): void
    {
        $this->idType = ucfirst($idType);
    }

    public function getIntId(): ?int
    {
        return $this->intId;
    }
    public function setIntId(int $intId): DataEventInterface
    {
        $this->intId = $intId;
        return $this;
    }

    public function getStrId(): ?string
    {
        return $this->strId;
    }
    public function setStrId(string $strId): DataEventInterface
    {
        $this->strId = $strId;
        return $this;
    }

    public function getParams(): ?array
    {
        return $this->params;
    }
    public function setParams(array $params): DataEventInterface
    {
        $this->params = $params;
        return $this;
    }
    public function addParam(string $name, $value): DataEventInterface
    {
        $this->params[$name] = $value;
        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }
    public function setMethod(string $method): DataEventInterface
    {
        $this->method = $method;
        return $this;
    }

    public function getObjectResult(): ?DataObjectInterface
    {
        return $this->objectResult;
    }
    public function setObjectResult(DataObjectInterface $objectResult): DataEventInterface
    {
        $this->objectResult = $objectResult;
        return $this;
    }

    public function getArrayResult(): ?array
    {
        return $this->arrayResult;
    }
    public function setArrayResult(array $arrayResult): DataEventInterface
    {
        $this->arrayResult = $arrayResult;
        return $this;
    }

    public function getIntResult(): ?int
    {
        return $this->intResult;
    }
    public function setIntResult(int $intResult): DataEventInterface
    {
        $this->intResult = $intResult;
        return $this;
    }

    public function getStrResult(): ?string
    {
        return $this->strResult;
    }
    public function setStrResult(string $strResult): DataEventInterface
    {
        $this->strResult = $strResult;
        return $this;
    }
}

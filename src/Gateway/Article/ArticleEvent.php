<?php

declare(strict_types=1);

namespace App\Gateway\Article;

use App\Gateway\DataEvent;

class ArticleEvent extends DataEvent {}

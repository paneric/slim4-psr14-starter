<?php

declare(strict_types=1);

namespace App\Gateway\Article;

use App\Gateway\DataListenerService;
use App\Interfaces\DataEventInterface;
use App\Interfaces\ArticleQueryInterface;
use App\Interfaces\ArticleRepositoryInterface;

class ArticleListenerService extends DataListenerService
{
    public function __construct(
        ArticleRepositoryInterface $articleRepositoryAdapter,
        ArticleQueryInterface $articleQueryAdapter
    ) {
        $this->repository = $articleRepositoryAdapter;

        $this->query = $articleQueryAdapter;
    }

    protected function queryAll(DataEventInterface $event): void
    {
        $this->query->adaptManager();

        $event->setArrayResult(
            $this->query->queryAll()
        );
    }
}

<?php

declare(strict_types=1);

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/Presentation/');

require ROOT_FOLDER . 'vendor/autoload.php';

use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Paneric\ModuleResolver\DefinitionsCollector;
use Paneric\ModuleResolver\ModuleResolver;
use Slim\Factory\AppFactory;

$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV',$_ENV['ENV']);

try {
    $moduleResolverConfig = require APP_FOLDER . 'config/module-resolver-config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolderName = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig
    );

    define('MODULE_FOLDER', APP_FOLDER . $moduleFolderName);

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        APP_FOLDER,
        MODULE_FOLDER,
        'config',
        'en',
        ENV
    );

    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(true);
    $builder->addDefinitions($definitions);

    $container = $builder->build();

    AppFactory::setContainer($container);
    $app = AppFactory::create();

    $container->set('base_path', ROOT_FOLDER);
    $container->set('route_parser_interface', $app->getRouteCollector()->getRouteParser());

    include MODULE_FOLDER . 'bootstrap/middleware.php';
    include APP_FOLDER . 'bootstrap/middleware.php';

    include APP_FOLDER . 'bootstrap/routes.php';
    include MODULE_FOLDER . 'bootstrap/routes.php';

    $app->run();

} catch (Exception $e) {
    echo $e->getMessage();
}

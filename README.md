# Slim4 - PSR14 starter

Modular and non concurrent event driven approach for Slim4 applications development with two exemplary responses for 
simple dao (article) and aggregate adao (article, credential).

## Few words of introduction

I am about to finish my recent e-commerce project, and I have to admit, I am sick and tired of incessant, complaining
remarks related to the number of parameters within my class constructors, especially within my controllers' related 
services.

Frequently, especially in case of different types of summary pages, there comes a need to deliver a huge amount of data 
fetched from different sources and sometimes the amount of necessary classes to create or inject becomes just 
overwhelming.

The above, plus horribly coupled presentation and domain layers make the final result hard to or absolutely non 
maintainable and completely non reusable.

Also, as I was always attracted by modular and decoupled project structuring, it was hard (at least for me) to achieve 
it decently.

Then I decided to make a use of PSR14...

## Packages

*  [php-di/php-di](http://php-di.org/doc/) - PSR-11 dependency injection container
*  [crel/tukio](https://github.com/Crell/Tukio) - PSR-14 event dispatcher implementation
*  [guzzlehttp/psr7](https://github.com/guzzle/psr7) - PSR-7 message implementation
*  [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv) - environment variables loader

and of course the one and only in its kind, irreplaceable:

*  [slim](http://www.slimframework.com/docs/v4/)

## Slim4 - PSR14 starter main goals

* modularity
* physical and functional separation of layers
    * **presentation**
    * **gateway**
    * **adapters**
    * **data access**
* reusability (both layers and its particular elements)

## Required

* PHP 7.3+

## Folders structure

* **slim4-psr14-starter**
    * public
    * sql
        * slim4-psr14-starter.sql *(data base starter)*
    * **src**
        * **Adapters**
            * **Article**
            * **Credential**                    
        * **DAL**
            * **Article**
            * **Credential**        
        * **Gateway**
            * **Article**
            * **Credential**         
        * **Interfaces**
        * **Presentation**
            * bootstrap
            * config
            * templates
            * **Commerce** *(exemplary module implementation)*
                * bootstrap
                * config
                * templates
                * **Controller**
                * **Handler**
                * **Service**
            
## Front controller index.php:

#### Resolving modules:

public/index.php
```php
<?php
...
    $moduleResolverConfig = require APP_FOLDER . 'config/module-resolver-config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolderName = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig
    );
...
```   
along with: 

src/Presentation/config/module-resolver-config.php
```php
<?php

declare(strict_types=1);

return [
    'default_route_key' => 'main',
    'local_map' => ['en', 'pl'],
    'module_map' => [
        'article' => 'Commerce',
        'cart' => 'Commerce',
    ],
];
```
The local_map contains accepted local values.

The module_map, by a simple rule, indicates (with the first element of a route pattern as a key) the module folder name.

src/Presentation/bootstrap/routes.php **and/or** src/Presentation/Commerce/bootstrap/routes.php
```php
<?php

declare(strict_types=1);

use App\Presentation\Commerce\Controller\ArticleController;
use App\Presentation\Commerce\Controller\CartController;

$app->get('/cart/show-all-articles', CartController::class . ':showAllArticles')
    ->setName('commerce.cart.show_all_articles');

$app->get('/article/show-all', ArticleController::class . ':showAll')
    ->setName('commerce.article.show_all');
```

#### Collecting configurations and dependency definitions:

public/index.php
```php
<?php
...
    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        APP_FOLDER,
        MODULE_FOLDER,
        'config',
        'en',
        ENV
    );
...
```

Loaded from:

* src/Presentation/config/dependencies/ *(common for all modules)*
* sre/Presentation/Commerce/config/dependencies/ *(appropriate for module)*

Files names are arbitrary.

#### Bootstrapping application and module:

public/index.php
```php
<?php
...
    include MODULE_FOLDER . 'bootstrap/middleware.php';
    include APP_FOLDER . 'bootstrap/middleware.php';

    include APP_FOLDER . 'bootstrap/routes.php';
    include MODULE_FOLDER . 'bootstrap/routes.php';
...
```
As presented, middlewares and routing bootstrapping is related to an application, and a module.
Application can be triggered only with general or modular configuration but also with both of them. It's up to you.

#### DEV, PROD, TEST modes:

Both:

* src/Presentation/config/settings/ *(common for all modules)*
* sre/Presentation/Commerce/config/settings/ *(appropriate for module)*

contains dev.php, prod.php test.php configurations loaded according to a constant value of ENV:

public/index.php
```php
<?php
...
$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV',$_ENV['ENV']);
...
```

## Application workflow:

![Workflow](./public/img/workflow.png "Workflow")

### Presentation layer:

![Presentation layer](./public/img/presentation_layer.png "Presentation layer")

#### (1) Service and handler:

Above schema presents base structure controller and controller related service but as you can see, instead of regular
repository or query injections there is CartHandler injected into service with PSR14 dispatcher as its parameter.

The CartHandler is responsible for triggering an event related to an appropriate repository or query, so instead of
calling repository method within CartService:

```php
<?php
...
    public function getAllArticles(): array
    {
        return [
            'articles' => $this->articleRepository->getAll()
        ];
    }
...
```
we call CartHandler:

src/Presentation/Commerce/Service/CartService.php
```php
<?php
...
    public function getAllArticles(): array
    {
        return [
            'articles' => $this->cartHandler->getAllArticles()
        ];
    }
...
```
It's looking almost the same, so what's the big deal ?
As I said at the beginning sometimes you need to get lots of staff, and you're forced inject quite a few 
repositories/queries into your controller related service. Using the handler (or however you call it) with an 
appropriate method name requires only one injection without taking care for a number of your data sources.

```php
<?php
...
    public function getAllArticles(): array
    {
        return [
            'articles' => $this->cartHandler->getAllArticles()
        ];
    }

    public function getAllOrders(): array
    {
        return [
            'orders' => $this->cartHandler->getAllOrders()
        ];
    }

    public function getAllClients(): array
    {
        return [
            'clients' => $this->cartHandler->getAllClients()
        ];
    }
...
```   

Still, with just one handler. Of course nothing stops you to create several handlers.


#### (2) Handler with PSR14 dispatcher:

Now the tricky part begins.

src/Presentation/Commerce/Handler/CartHandler.php
```php
<?php
...
    public function getAllArticles(): array
    {
        $articleDataEvent = new ArticleEvent();

        return $this->dispatcher->dispatch(
            $articleDataEvent->setMethod('queryAll')
        )->getArrayResult();
    }
...
```
As you can see our handler triggers an event with PSR14 dispatcher.
This is the moment when Presentation layer gets connected with DAL through the Gateway, but even though, there is
no injection from external layer because the event is simply instantiated within the method body. Event's parameters
will be discussed on the Gateway layer.

Apart from that, Presentation layer delivers appropriate interfaces to be implemented by repository/query adapters.

That's it. It's not perfect but looks quite well decoupled and easy to reuse.

### Gateway layer:

![Gateway_layer](./public/img/gateway_layer.png "Gateway layer")

#### (1) Event:

This is 100% custom class adapted for any particular situation. In this case it serves to send a request for data 
retrieval and getting a response. As the main goal is to trigger an appropriate method of repository/query adapter, so
event parameters are supposed to facilitate it in the easiest way possible:

src/Gateway/Article/ArticleEvent.php
```php
<?php
...
    protected $idType = 'Int';//string(Int,Str)

    protected $intId;//int
    protected $strId;//string

    protected $params;//array
    protected $method;//string

    protected $objectResult;//DataObjectInterface
    protected $arrayResult;//array
    protected $intResult;//int
    protected $strResult;//string
...
```
Let's say we want to fetch some articles by certain criteria, and our repository interface delivered by Presentation 
layer (that is supposed to be implemented by appropriate repository adapter) requires a method that follows:

src/Interfaces/ArticleRepositoryInterface.php
```php
<?php
...
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;
...
```
In this case mentioned above CartHandler has to create and set ArticleEvent for instance this way:

src/Presentation/Commerce/Handler/CartHandler.php
```php
<?php
...
    public function getArticlesBy(): array
    {
        $articleDataEvent = new ArticleEvent();
        $articleDataEvent->setMethod('findBy')
            ->setParams([
                'criteria' => ['some criteria'],
                'order_by' => ['id' => 'DESC'],
                'limit' => 10,
                'offset' => 10
            ]);

        return $this->dispatcher->dispatch($articleDataEvent)->getArrayResult();
    }
...
```

#### (2) Listener:

All dispatched events pass to their declared listeners through their execute() method:

src/Gateway/Article/ArticleListenerService.php
```php
<?php
...
    public function execute(DataEventInterface $event): void
    {
        $this->{$event->getMethod()}($event);
    }
...
```
Next an appropriate protected method is engaged, and the result is set back as event parameter:

src/Gateway/Article/ArticleListenerService.php
```php
<?php
...
    protected function findBy(DataEventInterface $event): void
    {
        $this->repository->adaptManager();

        $params = $event->getParams();

        $event->setArrayResult(
            $this->repository->findBy(
                $params['criteria'],
                $params['order_by'],
                $params['limit'],
                $params['offset']
            )
        );
    }
...
```
In this example ArticleListenerService is related to a single DAO, meaning Article through one repository and one query.
You can of course create more general listeners related to multiple DAOs but such approach will force you to invent a 
bit more sophisticated methods naming convention. 

#### (3) Attaching event to listener:

Simply through dependency definitions but just for a sake of clarity in a separate file:

src/Presentation/config/dependencies/psr14.php
```php
<?php
...
return [
    EventDispatcherInterface::class => static function (Container $container): Dispatcher
    {
        $provider = new OrderedListenerProvider($container);
        $provider->addListenerService(
            ArticleListenerService::class,
            'execute',
            ArticleEvent::class
        );

        return new Dispatcher($provider);
    },

    ArticleListenerService::class => static function (Container $container): ArticleListenerService
    {
        return new ArticleListenerService(
            $container->get(ArticleRepositoryInterface::class),
            $container->get(ArticleQueryInterface::class)
        );
    },
];
...
```

### Adapters layer:

![Adapters_layer](./public/img/adapters_layer.png "Adapters layer")

Different data source gathered with the different manners, this is the case where adapters become irreplaceable.
In case if it's data base or http client in most of the cases they implement different interfaces.
As it is advised to relay on interface names as DI container keys you cannot relay on autowiring, and you need to 
declare this kind of dependencies manually.

Here, as a common definition:

src/Presentation/config/dependencies/repository.php *(or any other convenient file name)*
```php
<?php
...
return [
    ArticleRepositoryInterface::class => static function (Container $container): ArticleRepositoryAdapter
    {
        return new ArticleRepositoryAdapter(
            $container->get(ArticleRepository::class)
        );
    },

    ArticleQueryInterface::class => static function (Container $container): ArticleQueryAdapter
    {
        return new ArticleQueryAdapter(
            $container->get(ArticleQuery::class)
        );
    },

    CredentialRepositoryInterface::class => static function (Container $container): CredentialRepositoryAdapter
    {
        return new CredentialRepositoryAdapter(
            $container->get(CredentialRepository::class)
        );
    },
];
...
```

or there, as module related definition:

src/Presentation/Commerce/config/dependencies/repository.php

### DAL layer:

This is a part totally up to you. ORM, DBAL or some http client. In this particular example data base conection configuration is
located in:

src/Presentation/config/settings/dev.php
```php
<?php

declare(strict_types=1);

return [
    'dbal' => [
        'limit' => 10,
        'host' => 'localhost',
        'charset' => 'utf8',
        'dbName' => 'slim4_psr14_starter',
        'user' => 'user',
        'password' => 'password',
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ],
    ],
];
```
and loaded in:

src/Presentation/config/dependencies/package.php
```php
<?php

declare(strict_types=1);

use DI\Container;
use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;

return [

    Manager::class => static function (Container $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },
];
```
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 08 Paź 2020, 07:22
-- Wersja serwera: 5.7.31-0ubuntu0.18.04.1
-- Wersja PHP: 7.3.22-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `slim4_psr14_starter`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles`
--

CREATE TABLE `articles` (
  `art_id` int(10) UNSIGNED NOT NULL,
  `art_credential_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `art_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `art_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `art_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `articles`
--

INSERT INTO `articles` (`art_id`, `art_credential_id`, `art_ref`, `art_created_at`, `art_updated_at`) VALUES
(1, 'a1b2c3', 'article1', '2020-10-07 18:48:01', '2020-10-07 18:48:01');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `credentials`
--

CREATE TABLE `credentials` (
  `crd_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crd_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `crd_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `credentials`
--

INSERT INTO `credentials` (`crd_id`, `crd_ref`, `crd_created_at`, `crd_updated_at`) VALUES
('a1b2c3', 'abc', '2020-10-07 18:46:06', '2020-10-07 18:46:06');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`art_id`),
  ADD UNIQUE KEY `unique_ref` (`art_ref`),
  ADD KEY `index_credential_id` (`art_credential_id`),
  ADD KEY `index_created_at` (`art_created_at`),
  ADD KEY `index_updated_at` (`art_updated_at`);

--
-- Indexes for table `credentials`
--
ALTER TABLE `credentials`
  ADD PRIMARY KEY (`crd_id`),
  ADD UNIQUE KEY `unique_ref` (`crd_ref`),
  ADD KEY `index_created_at` (`crd_created_at`),
  ADD KEY `index_updated_at` (`crd_updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `articles`
--
ALTER TABLE `articles`
  MODIFY `art_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`art_credential_id`) REFERENCES `credentials` (`crd_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
